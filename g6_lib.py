import socket
import socks
import zlib

content=[]
tor=False

def g6_reqStr(g6selector):
   return (g6selector+"\t\tD\r\n").encode()

def g6_reqCRC():
   return zlib.crc32("D".encode())

def g6_reqSend(address,port,g6selector):
   if tor or address[-6:].lower()==".onion":
      s = socks.socksocket()
      s.set_proxy(socks.SOCKS4, "localhost", 9050, True)
   else:
      s = socket.socket()
   s.connect((address, port))
#   print("s.connect")
   s.sendall(g6_reqStr(g6selector))
#   print(hex(g6_reqCRC()))
#   print("s.sendall")
   crc32=s.recv(4)
   if int.from_bytes(crc32,"little")==g6_reqCRC():
      status=int.from_bytes(s.recv(1),"little")
      gType=s.recv(1).decode()
      dataLen=int.from_bytes(s.recv(2),"little")
      return  (b'',s)
   else:
      return  (crc32,s)

def g6_request(address,port,g6selector):
   extra=g6_reqSend(address,port,g6selector)
   conn=extra[1]
   line=extra[0]
   while True:
       data = conn.recv(1)
       if not data:
           break
       line=line+data
   return line

def g6_split(data):
   data=data.decode("cp866").split("\n")
   data = [s.split("\r")[0].split("\t") for s in data]
   return data

def g6_printData(data):
   for s in data:
      print(s[0][1:])

def loadPage(address,port,g6selector):
   global content
   data=g6_request(address,port,g6selector)
   content=g6_split(data)

def printContent():
   global content
   for i in range(len(content)):
      line=content[i][0]
      if line=="":
        line="i"
      print(line[0],"{:2d}".format(i),line[1:])

def goLine(ans):
   global content
   line=content[ans]
   gType=line[0][0]
   if gType=="0" or gType=="1":
      loadPage(line[2],int(line[3]),line[1])

def homePage():
   global content
   content=g6_split('''
                                     LINKS
1Gopherpedia - A gopher interface to Wikipedia 	/	gopherpedia.com	70
1CNN Headline News	/cnn	codevoid.de	70
1SDF's Gopherhole - Public access Unix, lots of user phlogs	/	sdf.org	70
1Welcome to ForthWorks!		forthworks.com	70
1unclejonno - Techno-Minimalism	/users/unclejonno/	gopher.club	70
1Welcome to nihirash's gopher site!	/	nihirash.net	70
1World Of Spectrum gate(limited)	/zxdn.cgi	nihirash.net	70	+
1Weather maps and forecasts via Floodgap Groundhog(USA and Australia)	/groundhog	gopher.floodgap.com	70
1A link aggregator driven by the gopher community, for the gopher community.	/Port70News	1436.ninja	70
1yargo  # Yargo // HB9KNS #	/users/yargo/	gopher.club	70
1Hacker News Mirror		hngopher.com	70
1Bongusta! phlog aggregator	/bongusta/	i-logout.cz	70
1Floodgap File Archives and Mirrors	/archive	gopher.floodgap.com	70
11436.ninja - Raspberry Pi of Death Gopherhole - Wyoming Gopher Destination	/	1436.ninja	70
1EVERY GOPHER SERVER IN THE WORLD	/world	gopher.floodgap.com	70
1Magical.Fish - Fishin' for the best selectors in Gopherspace!	/	magical.fish	70
1Games!	/lawn/game	bitreich.org	70
1the gopher onion initiative	/onion	hg6vgqziawt5s4dj.onion	70
1Phourchan (iGopher-based 4chan archive.)	/phourchan	khzae.net	70
1Книги (Russian menu)	/ru/books	gopher.rp.spb.su	70
1UPDATE THE ACID	/	qcyjtksieyhjejyxff7itxk236icwchlateva7lv7vk6dztnld4m6tid.onion	70
1(You)r TFTP server	/index.gs	127.0.0.1	tftp:69
DCORONA VIRUS CLOCK	/corona/corona.G6D	qcyjtksieyhjejyxff7itxk236icwchlateva7lv7vk6dztnld4m6tid.onion	70
1OUR TOR BOARD	/board/	qcyjtksieyhjejyxff7itxk236icwchlateva7lv7vk6dztnld4m6tid.onion	70
'''.encode("cp866"))
