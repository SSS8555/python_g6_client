import socks
import zlib

def g6_reqStr(g6selector):
   return (g6selector+"\t\tD\r\n").encode()

def g6_reqCRC():
   return zlib.crc32("D".encode())

def g6_reqSend(address,port,g6selector):
   s = socks.socksocket()
   s.set_proxy(socks.SOCKS4, "localhost", 9050, True)
   s.connect((address, port))
#   print("s.connect")
   s.sendall(g6_reqStr(g6selector))
#   print(hex(g6_reqCRC()))
#   print("s.sendall")
   crc32=s.recv(4)
   if int.from_bytes(crc32,"little")==g6_reqCRC():
      status=int.from_bytes(s.recv(1),"little")
      gType=s.recv(1).decode()
      dataLen=int.from_bytes(s.recv(2),"little")
      return  (b'',s)
   else:
      return  (crc32,s)

def g6_request(address,port,g6selector):
   extra=g6_reqSend(address,port,g6selector)
   return  extra[0]+extra[1].recv(4096)

def g6_split(data):
   data=data.decode("cp866").split("\n")
   data = [s.split("\r")[0].split("\t") for s in data]
   return data

def g6_printData(data):
   for s in data:
      print(s[0][1:])


data=g6_request("qcyjtksieyhjejyxff7itxk236icwchlateva7lv7vk6dztnld4m6tid.onion",70,"/corona/corona.G6D")
dataM=g6_split(data)
print(dataM)

dataForm=g6_request(dataM[0][2],int(dataM[0][3]),dataM[0][1])
g6_printData(g6_split(dataForm))

dataEx=g6_reqSend(dataM[1][2],int(dataM[1][3]),dataM[1][1])
print(dataEx[0].decode(), end = '')
conn=dataEx[1]
while True:
    data = conn.recv(1)
    if not data:
        break
    print(data.decode(), end = '')
